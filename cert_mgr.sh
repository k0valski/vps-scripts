#!/bin/bash

ACME_SH_BASE="$HOME/.acme.sh"
ACME_SH="${ACME_SH_BASE}/acme.sh"
dhparams="/etc/nginx/dhparams"
certificate_base="/etc/letsencrypt"
config_file="/etc/common/domains"

domain=""
renew="true"
subdomains=()
force="false"

while [[ $# -gt 0 ]]; do
    case $1 in
        -d | --domain)
            domain="$2"
            shift 2
            ;;
        -s | --subdomain)
            subdomains+=("$2")
            shift 2
            ;;
        -i | --issue)
            renew="false"
            shift
            ;;
        -f | --force)
            force="true"
            shift
            ;;
        -*)
            echo "Unknown option $1"
            shift
            ;;
        *)
            echo "Unknown positional arg $1"
            shift # past argument
            ;;
    esac
done

if [[ -z $domain ]]; then
    echo "E: No domain given. domain=$domain"
    exit 1
fi

#common_args+=" --debug 3"
common_args=("--server" "letsencrypt"
    "--webroot" "/opt/acme-challenge"
    "--keylength" "4096"
    "--log")

domain_args=()

cert_args=(--key-file "$certificate_base/$domain/key.pem"
    --ca-file "$certificate_base/$domain/ca.pem"
    --cert-file "$certificate_base/$domain/cert.pem"
    --fullchain-file "$certificate_base/$domain/fullchain.pem"
    --reloadcmd "/usr/bin/systemctl restart nginx.service")

if [[ $force == "true" ]]; then
    common_args+=("--force")
fi

domain_args+=(-d "$domain")
for s in "${subdomains[@]}"; do
    domain_args+=("-d" "${s}.${domain}")
done

if [[ -f $config_file ]]; then
    for d in $(tr '\n' ' ' < $config_file); do
        if [[ -z $d ]]; then
            continue
        fi
        domain_args+=("-d" "${d}.${domain}")
    done
fi

if [[ ! -d $ACME_SH_BASE ]]; then
    (curl https://get.acme.sh | sh) || exit 1
fi

if [[ -f "$ACME_SH_BASE"/acme.sh.env ]]; then
    # shellcheck source=/dev/null
    . "$ACME_SH_BASE"/acme.sh.env
else
    echo "E: Could not source acme.sh.env"
    exit 1
fi

echo "Debug Output:"
echo "common_args:" "${common_args[@]}"
echo "domain_args:" "${domain_args[@]}"
echo "cert_args:" "${cert_args[@]}"

"$ACME_SH" --set-default-ca --server letsencrypt

echo "trying to renew certificate at ${certificate_base}"

if [[ $renew == "false" ]]; then
    if [ ! -d "$certificate_base/$domain" ]; then
        mkdir -p "$certificate_base/$domain"
    fi
    chown -R www-data:www-data "$certificate_base/$domain"
    chmod -R 775 "$certificate_base/$domain"

    if ! "${ACME_SH}" --issue "${domain_args[@]}" "${common_args[@]}" "${cert_args[@]}"; then
        echo "E: Could not issue certificate."
        exit 1
    fi
else
    if ! "$ACME_SH" --renew "${domain_args[@]}" "${common_args[@]}" "${cert_args[@]}"; then
        echo "E: Certificate renewal failed"
    fi

    if ! "$ACME_SH" --install-cert "${domain_args[@]}" "${cert_args[@]}"; then
        echo "E: Certificate installation failed"
    fi
fi

# Create DH parameters
if [[ ! -d $dhparams ]]; then
    mkdir -p "$dhparams"
    openssl dhparam -out "$dhparams/dhparams.pem" 4096
fi
